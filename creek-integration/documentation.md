#Caveats

You must disable the comments for each page. In order to render a post, the Creek plugin creates a single placeholder post that handles all of the themes.


#Tags


## Show index or schedule

You can use the `{cfm-show-schedule}` tag.


## Show pages

{cfm-title}
{cfm-full-description}
{cfm-airtimes}
{cfm-hosts}


## Broadcast pages

{cfm-title}
{cfm-show}
{cfm-show-title}
{cfm-date-time}
{cfm-main-text}
{cfm-media-audio}
{cfm-media-images}
{cfm-media-video}
{cfm-media} (all media)


## Profile pages

{cfm-title}
{cfm-main-text}
{cfm-media-audio}
{cfm-media-images}
{cfm-media-video}
{cfm-media} (all media)


## Multiple pages

### Shows, Broadcasts, Profiles

{cfm-image} (original size)
{cfm-image-sm}
{cfm-image-md}
{cfm-image-lg}

{cfm-image-div} (original size)
{cfm-image-sm-div}
{cfm-image-md-div}
{cfm-image-lg-div}