<?php


class Creek {

  public static $api_data_container = array();

  //Render a page's contents
  public static function render($identifier, $route){
    $data = self::get_api($identifier, $route);
    $html = preg_replace( "/\r|\n/", "", $data['html']);
    return $html;
  }

  //Get data from API
  public static function get_api($identifier, $route, $api_path = '') {

    $page_options = get_option('creek_page_options');

    //First check this route's API data has not already been
    // gathered for this route.
    // If so, then just return it right now.
    if(isset(self::$api_data_container[$route])){
      return self::$api_data_container[$route];
    }

    //Get domain setting from WordPress admin settings panel
    $creek_station_info = get_option('creek_station_info');
    $creek_domain = isset($creek_station_info['domain']) ? $creek_station_info['domain'] : false;
    if(!$creek_domain){
      return array('error' => "Domain not set up. Please edit Creek plugin settings.");
    }

    // Filter/escape HTML in query
    $identifier = esc_html($identifier);

    //just to test load speed without calling remote website

    if($api_path){
      $path = $api_path;
    }else{
      $path = CreekTemplates::$pages[$route]['path'].$identifier;
    }

    $url = "http://".$creek_domain.$path.'?json_wrap=1&wordpress=1&external=1';

    if($page_options["audio_inline_on_shows"]){
      $url .= "&audio_inline=1";
    }
    if($page_options["audio_player_text_color"] == "dark"){
      $url .= "&audio_player_text_color=dark";
    }
    // echo $url;

    // This doesn't actually catch or do anything
    try{
      $json = file_get_contents($url);
      $json_parsed = json_decode($json, true);
    }
    catch(Exception $e){

    }

    // print_r(htmlentities($json_parsed['html']));

    // Test JSON.
    // $json_parsed = ["title"=>"nope", "html"=>"nope"];

    self::$api_data_container[$route] = $json_parsed;

    return self::$api_data_container[$route];

    // return $json_parsed;

  }

}
