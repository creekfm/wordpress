<?php

class Creek_Broadcast {

	public static function init() {

		add_action('wp_router_generate_routes', array(get_class(), 'generate_routes'), 10, 1);

	}

	public static function generate_routes( WP_Router $router ) {
		$router->add_route('creek-router-broadcasts', array(
			'path' => '^broadcasts/(.*?)$',
			'query_vars' => array(
				'broadcast_id' => 1,
			),
			'page_callback' => array(get_class(), 'get_content'),
			'page_arguments' => array('broadcast_id'),
			'access_callback' => TRUE,
			'title_callback' => array(get_class(), 'get_title'),
			'title_arguments' => array('broadcast_id'),
			// 'template' => array('template-creek-show.php', dirname(__FILE__).DIRECTORY_SEPARATOR.'template-creek-show.php')
		));
	}

	public static function get_title( $argument ) {

		$api = Creek::get_api($argument, 'broadcast');

		return $api['title'];

	}

	public static function get_content( $argument ) {

		// print_r(Creek::get_api($argument, 'broadcast'));

		// echo htmlentities(Creek::render($argument, 'broadcast'));
		return Creek::render($argument, 'broadcast');
		// echo Creek::render($argument, 'broadcast');

	}

}
