<?php
/*
Plugin Name: Creek Integration
Plugin URI: https://github.com/creekfm/wordpress
Description: Displays content from a Creek station in a WordPress website.
Author URI: http://creek.fm/
Version: 0.1
*/


/*

//Copyright notice for WP Router Plugin.
// (The Creek plugin's page routing is based on WP Router.)

Copyright (c) 2012 Flightless, Inc. http://flightless.us/

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


/**
 * Load all the plugin files and initialize appropriately
 *
 * @return void
 */
if ( !function_exists('WP_Router_load') ) {

	function Check_WP_Page(){
		return (
			!is_admin()
			&& !empty($GLOBALS['pagenow'])
			&& $GLOBALS['pagenow'] != 'wp-login.php'
			&& $GLOBALS['pagenow'] != 'wp-register.php'
		);
	}

	function WP_Router_load() {

		// load the base class
		require_once 'WP_Router_Utility.class.php';

		if ( WP_Router_Utility::prerequisites_met(phpversion(), get_bloginfo('version')) ) {

			// WP_Router: Load all supporting files and hook into WordPress.
			require_once 'WP_Router.class.php';
			require_once 'WP_Route.class.php';
			require_once 'WP_Router_Page.class.php';

			//-----------------------------------------------
			// The Creek starts here.
			//

			require_once 'CreekTemplates.class.php';
			require_once 'Creek.class.php';
			require_once 'CreekSettingsPage.class.php';

			// Creek: Add global JS and CSS to the public website (but not admin).
			//
			if(Check_WP_Page()){

				// WordPress vars:
				// First, assemble some variables from Creek plugin settings
				//
				$page_options = get_option('creek_page_options');
				$station_options = get_option('creek_station_info');
				$ssl_enable = !empty($station_options["ssl_enable"]) ? true : false;
				$persistence_enable = !empty($page_options["persistence_enable"]) ? true : false;
				$persistence_container = !empty($page_options["persistence_container"]) ? $page_options["persistence_container"] : '';
				$persistence_type = !empty($page_options["persistence_type"]) ? $page_options["persistence_type"] : '';
				$audio_toolbar_enable = !empty($page_options["audio_toolbar_enable"]) ? $page_options["audio_toolbar_enable"] : 0;
				$audio_player_background_color = !empty($page_options["audio_player_background_color"]) ? $page_options["audio_player_background_color"] : "";
				$audio_player_text_color = !empty($page_options["audio_player_text_color"]) ? $page_options["audio_player_text_color"] : "";
				$css_custom = !empty($page_options["css_custom"]) ? $page_options["css_custom"] : "";
				$javascript_custom = !empty($page_options["javascript_custom"]) ? $page_options["javascript_custom"] : "";

				$options = array(
					'station_domain' => $station_options['domain'],
					'station_id' => $station_options['unique_id'],
					'ssl_enable' => $ssl_enable,
					'soundManager2_url' => plugin_dir_url(__FILE__ ).'assets/sm2/',
					'persistence_container' => '#'.$persistence_container,
					'persistence_type' => $persistence_type,
					'persistence_enable' => $persistence_enable,
					'audio_toolbar_enable' => $audio_toolbar_enable,
					'audio_player_background_color' => $audio_player_background_color,
					'audio_player_text_color' => $audio_player_text_color,
					'css_custom' => $css_custom,
					'javascript_custom' => $javascript_custom
				);

				//Add options to JS
				wp_register_script('js-creek_init_vars', plugin_dir_url(__FILE__).'assets/js/blank.js');
				wp_localize_script('js-creek_init_vars', 'wordpress_options',  $options);
				wp_enqueue_script('js-creek_init_vars');

				// Main Creek JS init file
				// This file contains the initial objects for Creek
				wp_enqueue_script( 'creek-main',  'https://cdn.creek.fm/embed/0.7.4-alpha/creek.js' );

				//Inject settings from WordPress
				wp_enqueue_script( 'creek-wordpress',  'https://cdn.creek.fm/embed/0.7.4-alpha/js/creek-wordpress.js', array('creek-main') );

				//Add theme
				wp_enqueue_style( 'creek-theme',  'https://cdn.creek.fm/embed/0.7.4-alpha/css/light1.css' );

			}

			add_action('init', array('WP_Router_Utility', 'init'), -100, 0);
			add_action(WP_Router_Utility::PLUGIN_INIT_HOOK, array('WP_Router_Page', 'init'), 0, 0);
			add_action(WP_Router_Utility::PLUGIN_INIT_HOOK, array('WP_Router', 'init'), 1, 0);

			//Insert the main routes for Creek
			require_once 'Creek_Show.class.php';
			require_once 'Creek_Broadcast.class.php';
			require_once 'Creek_Host.class.php';
			add_action(WP_Router_Utility::PLUGIN_INIT_HOOK, array('Creek_Show', 'init'), 1, 0);
			add_action(WP_Router_Utility::PLUGIN_INIT_HOOK, array('Creek_Broadcast', 'init'), 1, 0);
			add_action(WP_Router_Utility::PLUGIN_INIT_HOOK, array('Creek_Host', 'init'), 1, 0);

			//Add Settings link to the listing in the WordPress plugins page
			add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'add_action_links' );

			function add_action_links ( $links ) {
			 $mylinks = array(
			 '<a href="' . admin_url( 'options-general.php?page=creek-integration' ) . '">Settings</a>',
			 '<a href="http://creek.fm" target="_blank">Help</a>',
			 );
			return array_merge( $links, $mylinks );
			}

		} else {
			// let the user know prerequisites weren't met
			add_action('admin_head', array('WP_Router_Utility', 'failed_to_load_notices'), 0, 0);
		}
	}
	// Fire it up!
	WP_Router_load();
}
