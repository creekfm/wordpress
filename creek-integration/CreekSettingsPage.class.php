<?php


class CreekSettingsPage
{

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin',
            'Creek',
            'manage_options',
            'creek-integration',
            array( $this, 'create_admin_page' )
        );
    }


    /*
    <div class="Creek_airtimes">{cfm-airtimes}</div>
    <div class="Creek_hosts">{cfm-hosts}</div>
    <div class="Creek_full-description">{cfm-full_description}</div>
    */


    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'creek_station_info' );
        $this->page_options = get_option( 'creek_page_options' );

        ?>
        <div class="wrap">
            <h2>Creek</h2>
            <p>Settings for integration with <a href="http://creek.fm" target="_blank">Creek</a>.</p>
            <a href="http://hq.creek.fm" target="_blank" style="display:inline-block; margin:0 0 30px 0; border:2px solid rgba(0,0,0,.2); border-radius:6px; padding:6px 10px; text-decoration:none;">Edit station content at Creek.</a>
            <br>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'creek_settings' );
                do_settings_sections( 'creek-integration' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {


        //Settings: Station info

        register_setting(
            'creek_settings', // Option group
            'creek_station_info', // Option name
            // 'creek_station_info', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );
        add_settings_section(
            'section_creek_info', // ID
            'Station', // Title
            array( $this, 'print_section_info_station' ), // Callback
            'creek-integration' // Page
        );

        add_settings_field(
            'domain',
            'Domain',
            array( $this, 'domain_callback' ),
            'creek-integration',
            'section_creek_info'
        );
        add_settings_field(
            'unique_id',
            'Unique ID',
            array( $this, 'unique_id_callback' ),
            'creek-integration',
            'section_creek_info'
        );
        add_settings_field(
            'ssl_enable',
            'SSL: Enable',
            array( $this, 'ssl_enable_callback' ),
            'creek-integration',
            'section_creek_info'
        );

        //Settings: Page options

        //Register creek_page_options
        register_setting(
            'creek_settings', // Option group
            'creek_page_options' // Option name
            // 'creek_station_info', // Option name
            // array( $this, 'sanitize' ) // Sanitize
        );
        //Add page options section
        add_settings_section(
            'section_creek_page_options', // ID
            'Page Options', // Title
            array( $this, 'print_section_info_page_options' ), // Callback
            'creek-integration' // Page
        );

        add_settings_field(
            'debug_enable',
            'Debug',
            array( $this, 'debug_enable_callback' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'persistence_enable',
            'Persistence',
            array( $this, 'persistence_enable_callback' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'persistence_type',
            'Persistence: Type',
            array( $this, 'persistence_type_callback' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'persistence_container',
            'Persistence: Container',
            array( $this, 'persistence_container_callback' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'audio_player_background_color',
            'Audio Players: Background color',
            array( $this, 'audio_player_background_color' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'audio_player_text_color',
            'Audio Players: Text color',
            array( $this, 'audio_player_text_color' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'audio_toolbar_enable',
            'Audio Toolbar: Enable',
            array( $this, 'audio_toolbar_callback' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'audio_inline_on_shows',
            'Audio on Show Pages: Enable',
            array( $this, 'audio_inline_on_shows_callback' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'css_custom',
            'Custom CSS',
            array( $this, 'css_custom' ),
            'creek-integration',
            'section_creek_page_options'
        );
        add_settings_field(
            'javascript_custom',
            'Custom javaScript',
            array( $this, 'javascript_custom' ),
            'creek-integration',
            'section_creek_page_options'
        );

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

        if( isset( $input['domain'] ) )
            $new_input['domain'] = sanitize_text_field( $input['domain'] );

        if( isset( $input['unique_id'] ) )
            $new_input['unique_id'] = sanitize_text_field( $input['unique_id'] );

        // print_r($input);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info_station()
    {
        print 'This information hooks up WordPress with your station.';
    }
    public function print_section_info_page_options()
    {
        print 'These are options for the embedded tools.';
    }


    /**
     * Get the settings option array and print one of its values
     */
    public function domain_callback()
    {
        printf(
            '<input type="text" id="domain" name="creek_station_info[domain]" value="%s" />',
            isset( $this->options['domain'] ) ? esc_attr( $this->options['domain']) : ''
        );
    }
    public function unique_id_callback()
    {
        printf(
            '<input type="text" id="unique_id" name="creek_station_info[unique_id]" value="%s" />',
            isset( $this->options['unique_id'] ) ? esc_attr( $this->options['unique_id']) : ''
        );
    }
    public function ssl_enable_callback()
    {

        if(
          //Default to checked if not yet set
          !isset( $this->page_options['ssl_enable'] )
          || (
            isset( $this->page_options['ssl_enable'] )
            && $this->page_options['ssl_enable'])
          )
            $checked = ' checked="checked"';
        else
            $checked = '';

        echo '<input type="hidden" name="creek_station_info[ssl_enable]" value="0" /><input type="checkbox" id="ssl_enable" name="creek_station_info[ssl_enable]" '.$checked.' /><p>Default: <b>Enabled.</b> Disable this if your station has a custom domain, but does <b>not</b> have HTTPS (an SSL certificate).</p>';
    }
    public function debug_enable_callback()
    {
      if(isset( $this->page_options['debug_enable'] ) && $this->page_options['debug_enable'])
          $checked = ' checked="checked"';
      else
          $checked = '';

        echo '<input type="hidden" name="creek_station_info[debug_enable]" value="0" /><input type="checkbox" id="debug_enable" name="creek_station_info[debug_enable]" '.$checked.' /><p>Default: <b>Disabled.</b> </p>';
    }
    public function persistence_enable_callback()
    {
        if(isset( $this->page_options['persistence_enable'] ) && $this->page_options['persistence_enable'])
            $checked = ' checked="checked"';
        else
            $checked = '';

        echo '<input type="hidden" name="creek_page_options[persistence_enable]" value="0" /><input type="checkbox" id="persistence" name="creek_page_options[persistence_enable]" '.$checked.' /><p>Keeps audio playing even while users browse your website and navigate between pages.</p>';
    }
    public function persistence_type_callback()
    {
        printf(
            '<input type="text" id="persistence_type" name="creek_page_options[persistence_type]" value="%s" /><p><b>singlepage</b> or <b>iframe</b></p>',
            isset( $this->page_options['persistence_type'] ) ? esc_attr( $this->page_options['persistence_type']) : ''
        );
    }
    public function persistence_container_callback()
    {
        printf(
            '<input type="text" id="persistence_container" name="creek_page_options[persistence_container]" value="%s" /><p>Only for <b>singlepage</b> type.</p>',
            isset( $this->page_options['persistence_container'] ) ? esc_attr( $this->page_options['persistence_container']) : ''
        );
    }
    public function audio_player_background_color()
    {
        printf(
            '<input type="text" id="audio_player_background_color" name="creek_page_options[audio_player_background_color]" value="%s" /><p>Enter a CSS-compatible color, like: #FFFFFF or rgba(0,0,0,0.2)</p>',
            isset( $this->page_options['audio_player_background_color'] ) ? esc_attr( $this->page_options['audio_player_background_color']) : ''
        );
    }
    public function audio_player_text_color()
    {
        printf(
            '<input type="text" id="audio_player_text_color" name="creek_page_options[audio_player_text_color]" value="%s" /><p>Options: <b>light</b> or <b>dark</b>.</p>',
            isset( $this->page_options['audio_player_text_color'] ) ? esc_attr( $this->page_options['audio_player_text_color']) : ''
        );
    }
    public function audio_toolbar_callback()
    {
        if(isset( $this->page_options['audio_toolbar_enable'] ) && $this->page_options['audio_toolbar_enable'])
            $checked = ' checked="checked"';
        else
            $checked = '';

        echo '<input type="hidden" name="creek_page_options[audio_toolbar_enable]" value="0" /><input type="checkbox" id="audio_toolbar_enable" name="creek_page_options[audio_toolbar_enable]" '.$checked.' />';
    }
    public function audio_inline_on_shows_callback()
    {
        if(isset( $this->page_options['audio_inline_on_shows'] ) && $this->page_options['audio_inline_on_shows'])
            $checked = ' checked="checked"';
        else
            $checked = '';

        echo '<input type="hidden" name="creek_page_options[audio_inline_on_shows]" value="0" /><input type="checkbox" id="checkbox-audio-on-shows" name="creek_page_options[audio_inline_on_shows]" '.$checked.' />';
    }
    public function css_custom()
    {
        echo '<textarea name="creek_page_options[css_custom]" value="0" style="width:100%; height:200px;"/>'.htmlentities( @$this->page_options['css_custom']).'</textarea><p>Add custom CSS to style the Creek elements.</p>';
    }
    public function javascript_custom()
    {
        echo '<textarea name="creek_page_options[javascript_custom]" value="0" style="width:100%; height:200px;"/>'.htmlentities( @$this->page_options['javascript_custom']).'</textarea><p>Add custom JavaScript to related to Creek integration. This does not affect the main Creek.init(...) function.</p>';
    }

}

if( is_admin() )
    $my_settings_page = new CreekSettingsPage();
