<?php

class Creek_Show {

	public static function init() {

		add_action('wp_router_generate_routes', array(get_class(), 'generate_routes'), 10, 1);

	}

	public static function generate_routes( WP_Router $router ) {
		$router->add_route('creek-router-shows', array(
			'path' => '^shows/(.*?)$',
			'query_vars' => array(
				'show_short_name' => 1,
			),
			'page_callback' => array(get_class(), 'get_content'),
			'page_arguments' => array('show_short_name'),
			'access_callback' => TRUE,
			'title_callback' => array(get_class(), 'get_title'),
			'title_arguments' => array('show_short_name'),
		));
	}

	public static function get_title( $argument ) {

		$api = Creek::get_api($argument, 'show');

		return $api['title'];
	}

	public static function get_content( $argument ) {

		return Creek::render($argument, 'show');

	}

}
