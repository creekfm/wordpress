<?php

class Creek_Host {

	public static function init() {

		add_action('wp_router_generate_routes', array(get_class(), 'generate_routes'), 10, 1);

	}

	public static function generate_routes( WP_Router $router ) {
		$router->add_route('creek-router-hosts', array(
			'path' => '^hosts|profiles/(.*?)$',
			'query_vars' => array(
				'user_id' => 1,
			),
			'page_callback' => array(get_class(), 'get_content'),
			'page_arguments' => array('user_id'),
			'access_callback' => TRUE,
			'title_callback' => array(get_class(), 'get_title'),
			'title_arguments' => array('user_id'),
		));
	}

	public static function get_title( $argument ) {

		$api = Creek::get_api($argument, 'host');

		// print_r($api);

		return $api['title'];
	}

	public static function get_content( $argument ) {

		return Creek::render($argument, 'host');

	}

}
