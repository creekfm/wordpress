<?php

class CreekTemplates {

  //Underscores rather than dashes because of WP convention.
  public static $pages = array(
    'show'=>array(
      'template'=>'shows_view',
      'template_file'=>'shows-view',
      'title'=>'Show Pages',
      'route'=>'show',
      'path'=>'/shows/',
      'api'=>'/api/show/'
    ),
    'broadcast'=>array(
      'template'=>'broadcasts_view',
      'template_file'=>'broadcasts-view',
      'title'=>'Broadcast Pages',
      'route'=>'broadcast',
      'path'=>'/broadcasts/',
      'api'=>'/api/broadcast/'
    ),
    'host'=>array(
      'template'=>'users_profiles',
      'template_file'=>'users-profiles',
      'title'=>'Host/DJ Pages',
      'route'=>'host',
      'path'=>'/profiles/',
      'api'=>'/api/host/'
    )
  );

}
