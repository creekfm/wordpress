# Creek WordPress Plugin

This plugin integrates Creek radio content and tools with WordPress websites.

[More about the Creek WordPress plugin.](http://help.creek.fm/wordpress)

### Installation

To install and configure the plugin, [see the documentation here.](http://help.creek.fm/wordpress)

### Credit

Uses the WP Router plugin by jbrinley:

- https://github.com/jbrinley/WP-Router
- https://wordpress.org/plugins/wp-router/
